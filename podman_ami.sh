# https://github.com/containers/podman/issues/8888

Amazon Linux does not have a versio of podman available. That is why I wanted to find out whether I could still get the installation running by putting up the question here - perhaps someone knows what broke the installation by following the docs...

amazon linux is a recompile of rhel7 so the centos7 steps work as-is for amazon linux.
I dont think amazon will entertain requests for their own build of podman.

@thomashoefkens-vw I tried the steps on a docker.io/library/amazonlinux container just now and it worked fine. Could you please give it another try?
i
@rhatdan Member rhatdan commented on Jan 18, 2021
There should be a podman 1.6.6 version of podman on RHEL7, Centos7 and Amazone Linux 7 then correct?

@thimslugga thimslugga commented on Jan 21, 2021
For rootless podman these steps should work. let me know if you run into any issues.

sudo amazon-linux-extras disable docker
sudo amazon-linux-extras install -y kernel-ng

sudo yum check-update
sudo yum install -y yum-utils yum-plugin-copr

sudo cat <<EOF > /etc/yum.repos.d/devel\:kubic\:libcontainers\:stable.repo
[devel_kubic_libcontainers_stable]
name=Stable Releases of Upstream github.com/containers packages (CentOS_7)
type=rpm-md
baseurl=https://provo-mirror.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/CentOS_7/
gpgcheck=0
gpgkey=https://provo-mirror.opensuse.org/devel:/kubic:/libcontainers:/stable/CentOS_7/repodata/repomd.xml.key
enabled=1
EOF

sudo yum copr enable -y lsm5/container-selinux

sudo yum check-update
sudo yum install -y podman slirp4netns

test ! -f /etc/containers/seccomp.json && \
  sudo wget https://raw.githubusercontent.com/docker/labs/master/security/seccomp/seccomp-profiles/default.json -O /etc/containers/seccomp.json

sudo grubby --update-kernel=ALL \
   --args="systemd.unified_cgroup_hierarchy=1 namespace.unpriv_enable=1 user_namespace.enable=1"

echo "user.max_user_namespaces=10000" | sudo tee /etc/sysctl.d/98-userns.conf

echo "$(id -un):100000:65536" | sudo tee -a /etc/subuid
echo "$(id -un):100000:65536" | sudo tee -a /etc/subgid

sudo yum install -y git-core autoconf gettext-devel automake libtool libxslt byacc libsemanage-devel

mkdir -pv ~/src && cd ~/src
git clone https://github.com/shadow-maint/shadow shadow-utils
cd shadow-utils
./autogen.sh --prefix=/usr/local
make -j $(nproc)

sudo cp src/newgidmap src/newuidmap /usr/local/bin/

sudo setcap cap_setuid+ep /usr/local/bin/newuidmap
sudo setcap cap_setgid+ep /usr/local/bin/newgidmap

sudo systemctl reboot
podman system migrate
podman version
podman run --rm -it hello-world:latest

